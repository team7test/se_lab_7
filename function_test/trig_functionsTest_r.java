/**
 * This program uses JUnit to test methods in trigFunctions.java. Radian values test.
 * @author project team - 10
 * @version 3.0
 * @date 16-11-2020
 */
package function_test;

import static org.junit.Assert.*;

import org.junit.Assert;
import org.junit.Test;

public class trig_functionsTest_r {
	trig_functions t = new trig_functions();
	double deci = 0.1; // delta value for precision check
	/**
	 * Following are the test cases for sin, cos and tan for different qudrants.The input is in radians. 
	 * Values are converted in the dedicated functions.
	 */
	@Test
	public void test_sine45_d() //45
	{
		Assert.assertEquals(Math.sin(45), t.siner(45), deci);
	}

	@Test
	public void test_cos45_d() //45
	{
		Assert.assertEquals(Math.cos(45), t.cosiner(45), deci);
	}

	@Test
	public void test_tan45_d() //45
	{
		Assert.assertEquals(Math.tan(45), t.tanr(45), deci);
	}
	

}
