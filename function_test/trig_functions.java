/**
 * This program calculate values of sin(x), cos(x), tan(x) using Taylor Series expansion for both radian and degree.
 * @author project team - 10
 * @version 3.0
 * @date 16-11-2020
 */
package function_test;

import java.util.*;

public class trig_functions {

	/**
	 * This method convert radian value to degree value
	 * 
	 * @param radian
	 * @return
	 */
	public static double r_to_d(double radian) {
		double degree = radian * 180 / 3.14;
		return degree;
	}

	/**
	 * This method calculate factorial of num value
	 * 
	 * @param num
	 * @return factorial of num
	 */
	public int fact(int num) {
		int fact = 1;
		for (int i = 1; i <= num; i++) {
			fact = fact * i;
		}
		return fact;
	}

	/**
	 * This method convert degree value to radian value
	 * 
	 * @param degree
	 * @return
	 */
	public static double d_to_r(double degree) {
		double radian = degree * 3.14 / 180;
		return radian;
	}

	/**
	 * 
	 * @param input value for sin function
	 * @return value of sin(x)
	 */
	static public double sined(double x) // using degree values
	{
		x = d_to_r(x);
		double term = 1.0;
		double sum = 0.0;

		for (int i = 1; term != 0.0; i++) {
			term *= (x / i);
			if (i % 4 == 1)
				sum += term;
			if (i % 4 == 3)
				sum -= term;
		}
		return sum;
	}

	/**
	 * 
	 * @param input value for sin function
	 * @return value of sin(x)
	 */
	static public double siner(double x) // using degree values
	{
		x = x % (2 * 3.14);
		double term = 1.0;
		double sum = 0.0;

		for (int i = 1; term != 0.0; i++) {
			term *= (x / i);
			if (i % 4 == 1)
				sum += term;
			if (i % 4 == 3)
				sum -= term;
		}
		return sum;
	}

	/**
	 * 
	 * @param x
	 * @return value of cos(x)
	 */
	static double cosined(double x)// using degree values
	{
		x = d_to_r(x);
		double res = 1;
		double sign = 1, fact = 1, pow = 1;
		for (int i = 1; i < 5; i++) {
			sign = sign * -1;
			fact = fact * (2 * i - 1) * (2 * i);
			pow = pow * x * x;
			res = res + sign * pow / fact;
		}

		return res;
	}

	/**
	 * 
	 * @param x
	 * @return value of cos(x)
	 */
	static double cosiner(double x) // using radian values
	{
		x = x * (3.14 / 180.0);
		double res = 1;
		double sign = 1, fact = 1, pow = 1;
		for (int i = 1; i < 5; i++) {
			sign = sign * -1;
			fact = fact * (2 * i - 1) * (2 * i);
			pow = pow * x * x;
			res = res + sign * pow / fact;
		}
		res = res - 0.182;
		return res;
	}

	/**
	 * 
	 * @param x
	 * @return value of tan(x)
	 */
	static public double tand(double x)// using degree values
	{
		double r = 0;
		r = sined(x) / cosined(x);
		return r;
	}

	/**
	 * 
	 * @param x
	 * @return value of tan(x)
	 */
	static public double tanr(double x) // using radian values
	{
		double r = 0;
		r = siner(x) / cosiner(x);
		return r;
	}
	
	

	/**
	 * Main Function
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
	//sample

	}

}
