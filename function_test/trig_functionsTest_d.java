/**
 * This program uses JUnit to test methods in trigFunctions.java.
 * @author project team - 10
 * @version 3.0
 * @date 16-11-2020
 */
package function_test;

import static org.junit.Assert.*;

import org.junit.Assert;
import org.junit.Test;

public class trig_functionsTest_d {
	trig_functions t = new trig_functions();
	double deci = 0.1; // delta value for precision check
	/**
	 * Following are the test cases for sin, cos and tan for different qudrants.The input is in degrees. 
	 */
	@Test
	public void test_sine45_d() // 45
	{
		Assert.assertEquals(Math.sin(t.d_to_r(45)), t.sined(45), deci);
	}

	@Test
	public void test_cos45_d()//45
	{
		Assert.assertEquals(Math.cos(t.d_to_r(45)), t.cosined(45), deci);
	}

	@Test
	public void test_tan45_d() //45
	{
		Assert.assertEquals(Math.tan(t.d_to_r(45)), t.tand(45), deci);
	}
	
	@Test
	public void test_sine91_d() //91
	{
		Assert.assertEquals(Math.sin(t.d_to_r(91)), t.sined(91), deci);
	}

	@Test
	public void test_cos91_d() //91
	{
		Assert.assertEquals(Math.cos(t.d_to_r(91)), t.cosined(91), deci);
	}

	@Test
	public void test_tan91_d() //91
	{
		Assert.assertEquals(Math.tan(t.d_to_r(91)), t.tand(91), deci);
	}
	
	@Test
	public void test_sine181_d() //91
	{
		Assert.assertEquals(Math.sin(t.d_to_r(181)), t.sined(181), deci);
	}

	@Test
	public void test_cos181_d() //91
	{
		Assert.assertEquals(Math.cos(t.d_to_r(181)), t.cosined(181), deci);
	}

	@Test
	public void test_tan181_d() //91
	{
		Assert.assertEquals(Math.tan(t.d_to_r(181)), t.tand(181), deci);
	}


}
